﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class ScheduleService {
    constructor(private http: HttpClient) { }
	
	public dateFrom$: any = new Subject<any>();
	
	public modal$: any = new Subject<any>();
	
	public patient$: any = new Subject<any>();
	
	public patientModal$: any = new Subject<any>();
	
	public cell$: any = new Subject<any>();
	
	public hidden$: any = new Subject<any>();
	
	public newPatient$: any = new Subject<any>();
	
	public datePicker$: any = new Subject<any>();
	
	public checkedDoctors$: any = new Subject<any>();
	
	public deletePatient$: any = new Subject<any>();
	
	public doctorsString$: any = new Subject<any>();
	
	public resourcesIdsForSpec$: any = new Subject<any>();
	
	patient: any;
	
	patientModal: any;
	
	cell: any;

	public changeDate(date: any) {
		this.dateFrom$.next(date); 
	}
	
	public changeModal(id: any) {
		this.modal$.next(id); 
	}
	
	public changePatientModal(patientModal: any) {
		this.patientModal$.next(patientModal); 
		this.patientModal = patientModal;
	}
	
	public choosePatient(patient: any) {
		this.patient$.next(patient); 
		this.patient = patient;
	}
	
	public deletePatient(del: boolean) {
		this.deletePatient$.next(del); 
	}
	
	public chooseCell(cell: any) {
		this.cell$.next(cell);
		this.cell = cell;
	}
	
	public hiddenRightMenu(a: any) {
		this.hidden$.next(a);
	}
	
	public datePickerActive(picker: any) {
		this.datePicker$.next(picker); 
	}
	
	public doctorsCounter(count: any) {
		this.checkedDoctors$.next(count); 
	}
	
	public newPatient(newPatient: any) {
		this.newPatient$.next(newPatient);
	}
	
	public removeModalId() {
		this.modal$.next();
	}
	
	public doctorsString(doctorsString: any) {
		this.doctorsString$.next(doctorsString);
	}
	
	public resourcesIdsForSpec(resourcesIdsForSpec: any) {
		this.resourcesIdsForSpec$.next(resourcesIdsForSpec);
	}
	
	/* Данные методы нужны для компонентов, которые не появляются в DOM-дереве сразу
	(контексное меню, попапы), т.к. если создавать в них подписку на поток, то
	получать они будут только изменения, пока они висят в DOM, но не текущее значение. */	
	
	public getChoosedPatient() {
		return this.patient; 
	}
	
	public getChoosedCell() {
		return this.cell; 
	}
	
	public getPatientModal() {
		return this.patientModal; 
	}
	
	public getResources(dateForFilter: any, scheduleWD: any, doctorsString: any): Observable<any> {
        return this.http.get(`${environment.apiUrl}/schedule` + '/' + dateForFilter + '/' + scheduleWD + '/' + doctorsString);
    }
	
	public getAllPatients(value: any): Observable<any> {
        return this.http.get(`${environment.apiUrl}/patients/?search=` + value);
    }
	
	public getFilterResources(value: any): Observable<any> {
        return this.http.get(`${environment.apiUrl}/resources` + '/?search=' + value);
    }
	
	public getAvalibleDates(doctorsString: any): Observable<any> {
        return this.http.get(`${environment.apiUrl}/dates` + '/' + doctorsString);
    }
	
	public savePatientRecord (patient: any) {
        return this.http.post(`${environment.apiUrl}/save`, patient);
    }
	
	public deletePatientRecord (recId: any) {
        return this.http.post(`${environment.apiUrl}/delete`, recId);
    }
}