import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScheduleModule } from '@app/schedule/schedule.module';
import { SCHEDULE_ROUTES } from '@app/schedule/schedule.routes';

const routes: Routes = [
  {
    path: '',
	component: AppComponent,
	children: [
		{ path: '', redirectTo: 'schedule', pathMatch: 'full' },
		...SCHEDULE_ROUTES,
	  ]
	},
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
	RouterModule.forChild(routes),
	AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
	ScheduleModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
