﻿// import the required animation functions from the angular animations module
import { animate, state, style, transition, trigger } from '@angular/animations';

export const ToggleAnimation = trigger('ToggleAnimation', [
  state(
    'in',
    style({
      overflow: 'hidden',
      height: '*',
    })
  ),
  state(
    'out',
    style({
      opacity: '0',
      overflow: 'hidden',
      height: '0',
    })
  ),
  transition('in => out', animate('400ms ease-in-out')),
  transition('out => in', animate('400ms ease-in-out')),
]);
