import { Routes } from '@angular/router';
import { ScheduleComponentComponent } from '@app/schedule/schedule-component/schedule-component.component';

export const SCHEDULE_ROUTES: Routes = [{ path: 'schedule', component: ScheduleComponentComponent }];
