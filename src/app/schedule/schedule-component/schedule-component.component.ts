import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { ScheduleService } from '@app/_services/index';

@Component({
  selector: 'app-schedule-component',
  templateUrl: './schedule-component.component.html',
  styleUrls: ['./schedule-component.component.scss'],
})
export class ScheduleComponentComponent implements OnInit, OnDestroy {

  doctors: any = [];
  doctorsForFilter: any = [];
  data: any = [];
  dataForScheduleWorkDays: any = [];
  resources: any = [];
  filteredData: any = [];
  checked: any = [];
  resourceId: any;
  filteredOfDoctor: any = false;
  dateFrom: any;
  disableButtonGroup: boolean = true;
  scheduleWD: number = 1;
  targetChecked: boolean = false;
  targetId: number = 0;
  isScrollOffset: boolean = false;
  cell: any;
  pat: number = 0;
  newPatientRecord: any;
  preloader: boolean = false;
  doctorsString: string = '';
  
  lpuTimeFrom = 8;
  lpuTimeTo = 21;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private scheduleService: ScheduleService,
  ) {
	this.data = [];
  }

  ngOnInit(): void {
		// Здесь получаем список доступных ресурсов
		this.scheduleService.getFilterResources('').subscribe((resources: any) => {
			this.doctors = resources;
		});
		this.scheduleService.resourcesIdsForSpec$.subscribe((resourcesIdsForSpec: any) => {
			this.doctorsForFilter = resourcesIdsForSpec;
			if (this.doctorsForFilter.length !== 0) {
				this.scheduleService.datePickerActive(false);
			} else {
				this.scheduleService.datePickerActive(true);
			}
			this.scheduleService.doctorsCounter(this.doctorsForFilter.length);
			this.doctorsString = this.doctorsForFilter.join(',');
			this.scheduleService.doctorsString(this.doctorsString);
			this.filterData();
		})
		// Здесь отслеживаем изменение даты
		this.scheduleService.dateFrom$.subscribe((dateFrom: any) => {
			this.dateFrom = dateFrom;
			this.dateFrom = this.dateFrom.split('.');
			this.dateFrom = this.dateFrom.reverse().join('-');
			this.filterData();
		});
		// Здесь ловим ячейку и данные пациента для создания записи или ее удаления
		this.scheduleService.cell$.subscribe((cell: any) => {
			this.cell = cell;
		});
		this.scheduleService.newPatient$.subscribe((newPatientRecord: any) => {
			this.newPatientRecord = newPatientRecord;
			this.savePatientRecord(this.newPatientRecord, this.cell);
		});
		this.scheduleService.deletePatient$.subscribe((deletePatient: any) => {
			if (deletePatient) {
				this.deletePatientRecord(this.cell);
			}
		});
	}

  ngOnDestroy(): void {
	/* Здесь отписываемся от всех потоков. В текущей конфигурации это не сильно нужно, т.к.
	у нас всего одна страница, но если это будет юзаться,как npm-пакет в многостраничном приложении, 
	то от потоков нужно отписываться при удалении компонента из DOM-дерева, дабы они не висели в памяти. */
	this.scheduleService.dateFrom$.unsubscribe();
	this.scheduleService.newPatient$.unsubscribe();
	this.scheduleService.cell$.unsubscribe();
	this.scheduleService.deletePatient$.unsubscribe();
  }

  private filterData(): void {
	this.preloader = true;
	if (this.dateFrom && this.dateFrom.length !== 0) {
		let dateForFilter: any = (new Date(this.dateFrom)).toLocaleDateString();
		// Получаем данные для построения колонок расписания
		if (this.doctorsString.length !== 0) {
			this.scheduleService.getResources(dateForFilter, this.scheduleWD, this.doctorsString).subscribe((resources: any) => {
				this.resources = resources;
				// Данный массив выводится в колонки
				this.filteredData = [];
				// Формируем массив для вывода в колонки
				this.resources.forEach((entry: any) => {
					let doc: any = this.doctors.filter(function(f: any) {
					  return f.resourceId === entry.resourceId;
					});
					if (this.doctorsForFilter.length !==0) {
						this.filteredData.push(
							{
								'resourceId': doc[0].resourceId,
								'doctorId': doc[0].doctorId,
								'doctorName': doc[0].doctorName,
								'specialityId': doc[0].specialityId,
								'specialityName': doc[0].specialityName,
								'duration': doc[0].duration,
								'forHeader': this.dataForHeader(entry.timeQuotes),
								'cabNum': doc[0].cabNum,
								'lpuId': doc[0].lpuId,
								'lpuName': doc[0].lpuName,
								'resourceFromTime': new Date(this.resourceFromTime(entry.timeQuotes)),
								'resourceToTime': new Date(this.resourceToTime(entry.timeQuotes)),
								'sheduleDate': new Date(entry.timeQuotes[0].timeFrom),
								'scheduleList': this.countTimeQuotes(entry.timeQuotes, doc[0].duration, doc[0].resourceId)
							}
						)
					}
				})
				// Сортируем сформированный массив по возрастанию даты
				this.filteredData = this.filteredData.sort(function(a: any,b: any){
				  return a.sheduleDate - b.sheduleDate;
				});
				this.preloader = false;
			});
		} else {
			this.filteredData = [];
			this.doctorsForFilter = [];
		}
		this.disableButtonGroup = false;
	}
  }
  
  splitInterval(start: any, end: any, step: any) {
	let result = [];
	for (let ts = start; ts < end; ts += step) {
		result[result.length] = ts;
	}
	if (result.length == 1) {
		result[result.length] = end;
	}
	return result;
  }
  
  dataForHeader(quotesArray: any) {
	quotesArray = quotesArray.filter(function(f: any) {
	  return f.apointment === false;
	});
	return quotesArray
  }
  
  resourceFromTime(quotesArray: any) {
	quotesArray = quotesArray.filter(function(f: any) {
	  return f.apointment === true;
	});
	return quotesArray[0].timeFrom
  }
  
  resourceToTime(quotesArray: any) {
	quotesArray = quotesArray.filter(function(f: any) {
	  return f.apointment === true;
	});
	return quotesArray[0].timeTo
  }
  
  // Функция добавления ячеек в колонки
  countTimeQuotes(quote: any, duration: number, resourceId: number) {
	let q: any = [];
	quote = quote.sort(function(a: any,b: any){
	  return a.timeFrom - b.timeFrom;
	});
	let doc = this.doctors.filter(function(f: any) {
	  return f.resourceId === resourceId;
	});
	let patients: any = [];
	let continueTime: any = '';
	let forQuoteCrossing: any = [];
	let foQFilter: any = [];
	let endDay: any;
	quote.forEach((entry: any) => {
		/* Если рабочий день врача начинается позже начала времени работы ЛПУ, то добавляем ячейку на данный
		период, что врач не принимает*/
		if ((new Date(entry.timeFrom)).getHours() > this.lpuTimeFrom && q.length === 0) {
			let date: any = new Date(entry.timeFrom);
			date = date.setHours(date.getHours() - (new Date(entry.timeFrom).getHours() - this.lpuTimeFrom));
			q.push({
				'resourceId': resourceId,
				'doctorName': doc[0].doctorName,
				'cabNum': doc[0].cabNum,
				'timeFrom': new Date(date),
				'timeTo': new Date(entry.timeFrom),
				'apointment': false,
				'quoteStatus': 'Врач не принимает',
				'patients': [],
				'quoteCrossing': true
			})
		}
		// Формируем ячейки по умолчанию с шагом в длительность приема
		if (entry.apointment) {
			let start = (new Date(entry.timeFrom)).getTime();
			let end = (new Date(entry.timeTo)).getTime();
			continueTime = end;
			endDay = new Date(entry.timeTo);
			let step = duration * 60 * 1000;
			let result = this.splitInterval(start, end, step);
			let d: Date = new Date;
			for (let i = 0; i < result.length; ++i) {
			  d = new Date(result[i])
			  let d2 = new Date(result[i+1])
			  q.push({
				'resourceId': resourceId,
				'doctorName': doc[0].doctorName,
				'cabNum': doc[0].cabNum,
				'timeFrom': d,
				'timeTo': d2,
				'apointment': true,
				'quoteStatus': 'Принимает',
				'patients': [],
				'quoteCrossing': false,
				'tooltip': this.toolTips(d)
			  })
			}
		}
		// Если есть записанные пациенты - добавляем данные ячейки
		if (entry.patients.length !==0) {
			patients = entry.patients;
			for(let i = 0; i < patients.length; i++){
				if (i !== 0 && patients[i].timeFrom === patients[i-1].timeFrom) {
					patients[i-1].patientsCrossing = true;
					patients[i].patientsCrossing = patients[i-1].patientsCrossing
				}
			}
			let df: any = [];
			for (let i of entry.patients) {
				// Удаляем ячейки, сформированные по умолчанию, если их время совпадает со временем приема пациента
				q = q.filter(function(f: any) {
				  return f.timeFrom.toISOString() !== (new Date(i.timeFrom)).toISOString();
				});
			}
			entry.patients.forEach((entryPat: any) => {
				let p = [];
				p.push(entryPat)
				q.push({
					'resourceId': resourceId,
					'doctorName': doc[0].doctorName,
					'cabNum': doc[0].cabNum,
					'timeFrom': new Date(entryPat.timeFrom),
					'timeTo': new Date(entryPat.timeTo),
					'apointment': false,
					'quoteStatus': entryPat.shortName,
					'patients': p,
					'policy': entryPat.policy,
					'quoteCrossing': false,
					'patientsCrossing': entryPat.patientsCrossing
				})
				forQuoteCrossing.push(entryPat.patientId)
			})
		}
		// Добавляем закрытые периоды
		if (!entry.apointment) {
			let dFrom: Date = new Date(entry.timeFrom);
			let dTo: Date = new Date(entry.timeTo);
			foQFilter.push({
				'timeFrom': new Date(entry.timeFrom),
				'timeTo': new Date(entry.timeTo)
			})
			// Если ни один из записанных пациентов не попадает в закрытый для записи период
			if (patients.length === 0 || patients.length !== 0 && (new Date(patients[0].timeFrom) < dFrom || patients.length !== 0 && new Date(patients[0].timeFrom) > dTo)) {
				q.push({
					'resourceId': resourceId,
					'doctorName': doc[0].doctorName,
					'cabNum': doc[0].cabNum,
					'timeFrom':  new Date(entry.timeFrom),
					'timeTo':  new Date(entry.timeTo),
					'apointment': false,
					'quoteStatus': entry.quoteStatus,
					'patients': [],
					'quoteCrossing': false,
					'notFilter': true
				})
			}
			/* Если пациент попадает в закрытый период.
			Здесь поступаем следующим образом - разбиваем закрытый период на две ячейки и между ними помещаем
			ячейки с пациентами, запись которых попадает на данный период. При удалении данной записи ячейки будут*
			объединяться, эта логика прописана выше.*/
			if (patients.length !== 0 && (new Date(patients[0].timeFrom) >= dFrom && new Date(patients[0].timeTo) < dTo)) {
				q = q.sort(function(a: any,b: any) {
				  return a.timeFrom - b.timeFrom;
				});
				let pat = this.pat;
				q = q.filter(function(f: any) {
				  return f.timeFrom < new Date(entry.timeFrom) ||
				  f.timeFrom > new Date(entry.timeTo) ||
				  f.patients.length !== 0 && f.patients[0].patientId === pat;
				});
				if (forQuoteCrossing.length !== 0) {
					for(let item of forQuoteCrossing){
						q = q.filter(function(f: any) {
							return f.patients.patientId !== item
						})
					}
				}
				q.push({
					'resourceId': resourceId,
					'doctorName': doc[0].doctorName,
					'cabNum': doc[0].cabNum,
					'timeFrom': new Date(entry.timeFrom),
					'timeTo': new Date(patients[0].timeFrom),
					'apointment': false,
					'quoteStatus': entry.quoteStatus,
					'patients': [],
					'quoteCrossing': false,
					'notFilter': true
				})
				patients.forEach((entryPat: any) => {
					q.push({
						'resourceId': resourceId,
						'doctorName': doc[0].doctorName,
						'cabNum': doc[0].cabNum,
						'timeFrom': new Date(entryPat.timeFrom),
						'timeTo': new Date(entryPat.timeTo),
						'apointment': false,
						'quoteStatus': entryPat.shortName,
						'patients': patients,
						'policy': entryPat.policy,
						'quoteCrossing': true,
						'notFilter': true,
						'patientsCrossing': entryPat.patientsCrossing
					})
				})
				q.push({
					'resourceId': resourceId,
					'doctorName': doc[0].doctorName,
					'cabNum': doc[0].cabNum,
					'timeFrom': new Date(patients[0].timeTo),
					'timeTo': new Date(entry.timeTo),
					'apointment': false,
					'quoteStatus': entry.quoteStatus,
					'patients': [],
					'quoteCrossing': false,
					'notFilter': true
				})
			}
		}
	})
	/* Если рабочий день врача заканчивается раньше времени конца работы ЛПУ, то добавляем ячейку на данный
	период, что врач не принимает*/
	if ((new Date(q[q.length-1].timeFrom)).getHours() < this.lpuTimeTo) {
		q.push({
			'resourceId': resourceId,
			'doctorName': doc[0].doctorName,
			'cabNum': doc[0].cabNum,
			'timeFrom': endDay,
			'timeTo': endDay,
			'apointment': false,
			'quoteStatus': 'Врач не принимает',
			'patients': []
		})
	}
	// Сортируем полученный массив ячеек по возрастанию времени
	q = q.sort(function(a: any,b: any) {
	  return a.timeFrom - b.timeFrom;
	});
	foQFilter.forEach((entry: any) => {
		q = q.filter(function(f: any) {
		  return new Date(f.timeFrom) < entry.timeFrom || 
		  new Date(f.timeFrom) >= entry.timeTo || 
		  f.timeFrom.toLocaleTimeString() === entry.timeFrom.toLocaleTimeString() && !f.apointment || 
		  f.notFilter
		});
	})
	return q;
  }
  
  toolTips(d: Date) {
	let str = ''
	if (d < new Date()) {
		str = 'Данное время недоступно для записи'
	}
	if (d > new Date()) {
		str = 'Данное время доступно для записи'
	}
	return str
  }

  scheduleWorkDays(dayscounter: number): void {
    this.filteredData = [];
	this.scheduleWD = dayscounter;
	this.filterData();
  }
  
  // Функция сохранения записи пациента
  savePatientRecord(newPatientRecord: any, cell: any) {
	let hoursFrom = cell.timeFrom.getHours();
	let hoursTo = cell.timeTo.getHours();
    let minutesFrom = cell.timeFrom.getMinutes();
	let minutesTo = cell.timeTo.getMinutes();
	/* Данная конструкция из if-ов здесь потому что на сервер нужно отправлять строку со
	временем начала и окончания записи вида '10:00', хотя логичнее было бы отпраявлять строку с
	датой-временем в ISO-формате и тогда она была бы не нужна, 
	но так уж реализован бэкенд - он слеплен на скорую руку, т.к.
	задачи на его написание не стояло, да и делал его не я */
	if (cell.timeFrom.getHours() < 10) {
		hoursFrom = '0' + cell.timeFrom.getHours();
	}
	if (cell.timeTo.getHours() < 10) {
		hoursTo = '0' + cell.timeTo.getHours();
	}
	if (cell.timeFrom.getMinutes() === 0) {
		minutesFrom = '00';
	}
	if (cell.timeTo.getMinutes() === 0) {
		minutesTo = '00';
	}
	let record: any = {
		'patientId': newPatientRecord.patientId,
		'resourceId': newPatientRecord.resourceId,
		'date': cell.timeFrom.toLocaleDateString(),
		'timeFrom': hoursFrom + ':' + minutesFrom,
		'timeTo': hoursTo + ':' + minutesTo,
	}
	this.scheduleService.savePatientRecord(record).subscribe((response: any) => {
		this.filterData();
	});
  }
  
  // Удаление записи пациента
  deletePatientRecord(cell: any) {
	let recId: any = {
		'recId': cell.patients[0].recId
	}
	this.scheduleService.deletePatientRecord(recId).subscribe((response: any) => {
		this.filterData();
	});
  }
  
  hiddenRightMenu() {
	this.scheduleService.hiddenRightMenu(true);
  }

  onScroll(e: Event): void {
    this.isScrollOffset = !!((e.target as Element).scrollTop)
  }

}
