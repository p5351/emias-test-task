import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleComponentComponent } from './schedule-component/schedule-component.component';
import { DoctorsComponent } from '@app/components/doctors/doctors.component';
import { PatientsComponent } from '@app/components/patients/patients.component';
import { DatepickerComponent } from '@app/components/datepicker/datepicker.component';
import { ScheduleCardComponent } from '@app/components/schedule-card/schedule-card.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from '@app/angular-material.module';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { RightClickMenuComponent } from '@app/components/right-click-menu/right-click-menu.component';
import { ModalModule } from '@app/components/_modal';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    ScheduleComponentComponent,
	DoctorsComponent,
	PatientsComponent,
	DatepickerComponent,
	ScheduleCardComponent,
	RightClickMenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
	FormsModule,
	AngularMaterialModule,
	ModalModule,
	NgSelectModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'hi-IN'}
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ScheduleModule { }
