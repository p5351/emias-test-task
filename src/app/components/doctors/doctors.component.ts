﻿import { Component, OnInit, Input } from '@angular/core';
import { fadeInAnimation, ToggleAnimation } from '@app/_animations';
import { ScheduleService } from '@app/_services';

@Component({
	selector: 'app-doctors',
    templateUrl: 'doctors.component.html',
	animations: [fadeInAnimation, ToggleAnimation],
    host: { '[@fadeInAnimation]': '' },
})
export class DoctorsComponent implements OnInit {
	@Input() data: any;
	
	resources: any;
	doctorsList: string = 'out';
    panelOpenState = false;
	buttonDisabled = true;
	sortedForSpec: any = [];
	sortedSpec: boolean = false;
	specList: number = 0;
	check: number = 0;
	changedAll: boolean = false;
	changed: boolean = false;
	selectedResource: any = null;
	resourcesIdsForChangeSpec: any = [];
	
	constructor(
        private readonly scheduleService: ScheduleService
    ) {}
    
    ngOnInit() {
		this.scheduleService.dateFrom$.subscribe((dateFrom: any) => {
			this.buttonDisabled = false;
		});
		this.scheduleService.checkedDoctors$.subscribe((check: any) => {
			this.check = check;
		});
    }
	
	toggleList(event: any, item: any): void {
		this.panelOpenState = !this.panelOpenState;
		this.doctorsList = this.doctorsList === 'out' ? 'in' : 'out';
	}
	
	changeSpec(event: any, item: number) {
		this.specList = item;
		let fl = this.data.filter(function(f: any) {
		  return f.specialityId === item;
		});
		if (event.target.checked) {
			for (let i of fl) {
				i.checked = event.target.checked;
				this.resourcesIdsForChangeSpec.push(i.resourceId)
			}
		} else {
			for (let i of fl) {
				i.checked = event.target.checked;
				this.resourcesIdsForChangeSpec = this.resourcesIdsForChangeSpec.filter(function(f: any) {
				  return f !== i.resourceId;
				});
			}
		}
		this.resourcesIdsForChangeSpec = [...new Set(this.resourcesIdsForChangeSpec)]
		this.scheduleService.resourcesIdsForSpec(this.resourcesIdsForChangeSpec);
	}
	
	filterForId(event: any, item: number) {
		this.changed = true;
		for (let i of this.data) {
			if (i.resourceId === item) {
				i.checked = event.target.checked;
			}
		}
		if (event.target.checked) {
			this.resourcesIdsForChangeSpec.push(item)
		} else {
			if (this.sortedSpec) {
				let fss = this.data.filter(function(f: any) {
				  return f.resourceId === item;
				});
				let fl = this.data.filter(function(f: any) {
				  return f.specialityId === fss[0].specialityId;
				});
				(document.getElementById(fl[0].specialityId) as HTMLInputElement).checked = false;
			}
			this.resourcesIdsForChangeSpec = this.resourcesIdsForChangeSpec.filter(function(f: any) {
			  return f !== item;
			});
		}
		this.resourcesIdsForChangeSpec = [...new Set(this.resourcesIdsForChangeSpec)]
		this.scheduleService.resourcesIdsForSpec(this.resourcesIdsForChangeSpec);
	}
	
	changeAll(event: any, checked: any) {
		for (let i of this.data) {
			i.checked = checked;
			event.target.checked = checked;
			this.changedAll = checked;
			if (event.target.checked) {
				this.resourcesIdsForChangeSpec.push(i.resourceId)
			} else {
				this.resourcesIdsForChangeSpec = this.resourcesIdsForChangeSpec.filter(function(f: any) {
				  return f !== i.resourceId;
				});
			}
		}
		this.resourcesIdsForChangeSpec = [...new Set(this.resourcesIdsForChangeSpec)]
		this.scheduleService.resourcesIdsForSpec(this.resourcesIdsForChangeSpec);
	}
	
	sortForName() {
		this.scheduleService.resourcesIdsForSpec(this.resourcesIdsForChangeSpec);
		this.specList = 0;
		this.sortedSpec = false;
		this.data = this.data.sort(this.sortArrayForName);
	}
	
	sortForSpec() {
		this.scheduleService.resourcesIdsForSpec(this.resourcesIdsForChangeSpec);
		const specNames = Array.from(this.data, ({specialityName}) => specialityName);
		const uniqueSpec = Array.from(new Set(specNames));
		let sortedForSpec: any = [];
		for (let i of uniqueSpec) {
			let data = this.data.filter(function(f: any) {
			  return f.specialityName === i;
			});
			sortedForSpec.push(data)
		}
		this.sortedSpec = true;
		this.sortedForSpec = sortedForSpec;
	}
	
	sortArrayForName(x: any, y: any){
		if (x.doctorName < y.doctorName) {return -1;}
		if (x.doctorName > y.doctorName) {return 1;}
		return 0;
	}
	
	sortArrayForSpec(x: any, y: any){
		if (x.specialityName < y.specialityName) {return -1;}
		if (x.specialityName > y.specialityName) {return 1;}
		return 0;
	}
	
	inputName(value: any) {
		/* Данная проверка здесь потому что это условие не реализовано со стороны бэкенда,
		ибо он у нас простой,повторюсь. По ТЗ было условие, что поиск начинается с ввода 3 и
		более символов. В норме ее здесь быть не должно. Сервер должен в ответ на некорректный 
		query-параметр, в данном случае слишком короткое имя врача или название специальности, сам выдавать
		либо пустой массив, либо ошибку. */
		if (value.target.value.length >= 3) {
			this.scheduleService.getFilterResources(value.target.value).subscribe((resources: any) => {
				this.resources = resources;
			});
		}
		if (value.target.value.length < 3) {
			this.resources = [];
		}
	}
	
	/* Единственное назначение этой функции - отмечать галочкой ресурс, выбранный в селекторе.
	Если честно, то не совсем понимаю такого решения в плане интерфейса. в функции inputName мы хотя бы получаем 
	список доступных ресурсов, но в окно, в котором данная функция отмечает галочкой выбранный ресурс прилетает
	весь массив сразу. Он никак не фильтруется,не обновляется и прочее, потому что по ТЗ такого не было.
	Может логичнее было бы просто выбирать в селекторе ресурсы, ng-select, использованный для него, позволяет и множественный выбор,
	а данное окно скрыть за кнопкой типа Представить в виде дерева или что-то подобное и там уже тоже искать ресурсы по названию и специальности,
	обновлять их список и т.д., а не тупо пробрасывать в него событие, дабы просто была галочка рядом с выбранным ресурсом*/
	filteredList(event: any): void {
		if (event) {
			(document.getElementById(event.resourceId) as HTMLInputElement).click();
			this.resources = [];
		}
	}
	
}