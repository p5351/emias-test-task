import { Component, OnInit, Input } from '@angular/core';
import { ScheduleService } from '@app/_services';
import { ScheduleCardComponent } from '@app/components/schedule-card/schedule-card.component';

@Component({
  selector: 'app-right-click-menu',
  templateUrl: './right-click-menu.component.html',
  styleUrls: ['right-click-menu.component.scss']
})
export class RightClickMenuComponent implements OnInit {

  @Input() x = 0;
  @Input() y = 0;
  @Input() data: any;
  patient: any;
  
  public isHidden: boolean = true;
  
  currentTime = new Date();

  constructor(private readonly scheduleService: ScheduleService) { }

  ngOnInit(): void {
	this.patient = this.scheduleService.getChoosedPatient();
	this.data = this.scheduleService.getChoosedCell();
	if (this.data) {
		this.scheduleService.cell$.subscribe((cell: any) => {
			this.data = cell;
		});
	}
	if (this.patient) {
		this.scheduleService.patient$.subscribe((patient: any) => {
			this.patient = patient;
		});
	}
  }
  
  sendPatientInService(patient: any) {
	patient.resourceId = this.data.resourceId;
	patient.doctorName = this.data.doctorName;
	patient.cabNum = this.data.cabNum;
	this.scheduleService.changePatientModal(patient);
  }

  openModal(id: string) {
	this.close();
	if (this.data.patients.length !== 0) {
		this.sendPatientInService(this.data.patients[0])
	}
    this.scheduleService.changeModal(id);
	if (id === 'custom-modal-2') {
		let changedPatient = this.scheduleService.getChoosedPatient();
		let dataModal = this.scheduleService.getPatientModal();
		let cell = this.scheduleService.getChoosedCell();
		let cPatient: any = {
			'resourceId': cell.resourceId,
			'patientId': changedPatient.patientId,
			'patientName': changedPatient.shortName,
			'birthdate': changedPatient.birthdate,
			'policy': changedPatient.policy,
			'lpuId': changedPatient.lpuId,
			'timeFrom': cell.timeFrom, 
			'timeTo': cell.timeTo
		}
		this.scheduleService.newPatient(cPatient);
	}
  }
  
  close() {
	this.scheduleService.hiddenRightMenu(true);
  }
  
}
