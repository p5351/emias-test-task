﻿import {Component, OnInit, Input} from '@angular/core';
import {ToggleAnimation} from '@app/_animations';
import {ScheduleService} from '@app/_services';

import {DateAdapter} from '@angular/material/core';

@Component({
  selector: 'app-datepicker',
  templateUrl: 'datepicker.component.html',
  styleUrls: ['datepicker.component.scss'],
  animations: [ToggleAnimation],
})
export class DatepickerComponent implements OnInit {
  @Input() data: any;
  dateFrom: any = '';
  buttonDisabled = true;
  datesList: any = [];
  doctorsString: any = '';

  openedDates: any = []

  selectedDate: Date = new Date(this.dateFrom)

  constructor(
    private readonly scheduleService: ScheduleService,
    private dateAdapter: DateAdapter<any>
  ) {
  }

  datePick: string = 'in';
  panelOpenState = true;


  ngOnInit() {
	this.dateAdapter.setLocale('ru-RU');
	this.scheduleService.datePicker$.subscribe((picker: any) => {
		this.buttonDisabled = picker;
	});
	this.scheduleService.doctorsString$.subscribe((doctorsString: any) => {
		this.doctorsString = doctorsString;
		if (this.doctorsString.length !== 0) {
			this.scheduleService.getAvalibleDates(this.doctorsString).subscribe((datesList: any) => {
				this.datesList = datesList;
				this.dateFrom = datesList[0];
				if (new Date() > new Date(datesList[0])) {
					let date = new Date();
					this.dateFrom = date.toISOString();
				}
			});
		}
	});
  }

  toggleList(event: any, item: any): void {
    this.panelOpenState = !this.panelOpenState;
    this.datePick = this.datePick === 'in' ? 'out' : 'in';
  }

  pickDate(date: any) {
    this.scheduleService.changeDate((date.value).toLocaleDateString());
  }

  datesFilter = (d: any): any => {
	let datesList = this.datesList.filter(function filt(f: any) {
	  return new Date(f).toLocaleDateString() === d.toLocaleDateString();
	});
	if (d) {
		return d.toLocaleDateString() === (new Date(datesList[0])).toLocaleDateString();
	}
  }
}
