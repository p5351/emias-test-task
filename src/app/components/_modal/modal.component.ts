﻿import { Component, ViewEncapsulation, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';
import { fadeInAnimation } from '@app/_animations';
import { ModalService } from '@app/components/_modal';
import { ScheduleService } from '@app/_services';

@Component({
  selector: 'jw-modal',
  templateUrl: 'modal.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' },
})
export class ModalComponent implements OnInit, OnDestroy {
  @Input() id: string = '';
  private element: any;
  data: any;

  constructor(private modalService: ModalService, 
				private el: ElementRef,
				private readonly scheduleService: ScheduleService) {
    this.element = el.nativeElement;
  }

  ngOnInit(): void {
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }
    document.body.appendChild(this.element);
    this.element.addEventListener('click', (el: any) => {
      if (el.target.className === 'jw-modal') {
        this.close();
      }
    });
    this.modalService.add(this);
  }

  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  open(): void {
    this.element.style.display = 'block';
    document.body.classList.add('jw-modal-open');
  }

  close(): void {
    this.element.style.display = 'none';
    document.body.classList.remove('jw-modal-open');
  }
}
