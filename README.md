# Emias Test Task (Scheduler)

Устанавливаем Node.js(https://nodejs.org/en/).

# Установка Angular CLI

Устанавливаем [Angular CLI] `npm install -g @angular/cli`.

## Установка пакетов

Устанавливаем пакеты `npm install`.

## Запуск в dev-режиме

Команда `ng serve`, проект запустится по адресу `http://localhost:4200/`.

## Сборка проекта

Команда `ng build` для сборки на продакшн. Итоговый билд будет в папке `dist/emias`.

## Docker

Собираем image `docker build -t js/angular:1.0.0 .` и запускаем `docker run --rm -ti -p 8080:80 js/angular:1.0.0`
